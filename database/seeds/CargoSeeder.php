<?php

use App\cargo;
use Illuminate\Database\Seeder;

class CargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        cargo::create([
            "descripcion" => "cargo 1",
        ]); 
        cargo::create([
            "descripcion" => "cargo 2"
        ]); 
        cargo::create([
            "descripcion" => "cargo 3"
        ]); 
    }
}

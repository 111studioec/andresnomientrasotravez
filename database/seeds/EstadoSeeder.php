<?php

use App\Estado;
use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = Estado::create([
            "descripcion" => "estado 1",
        ]); 
        $id = Estado::create([
            "descripcion" => "estado 2",
            "estado_me_id" => $id->id
        ]); 
        $id = Estado::create([
            "descripcion" => "estado 3",
            "estado_me_id" => $id->id
        ]); 
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cargo extends Model
{
    protected $fillable = [
        'descripcion', 
        'estado_id', 
    ];
    public function estado()
    {
        return $this->belongsTo(Estado::class);
    }
}

@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card col-12 mt-5"> 
        <div class="card-header d-flex justify-content-between">
            Ver Empleado
            <a class="btn btn-default" href="{{ route("empleados.index" ) }}"  data-toggle="tooltip" title="Listado"><i class="fa fa-book"></i></a>
        </div>
        <div class="card-body"> 
            <div class="ruc">
                <strong>Cédula: </strong>{{ $empleado->cedula }}
            </div>
            <div class="nombre">
                <strong>Nombres: </strong>{{ $empleado->nombre1 }} {{ $empleado->nombre2 }} {{ $empleado->apellido1 }} {{ $empleado->apellido2 }}
            </div>
            <div class="correo">
                <strong>Correo: </strong>{{ $empleado->correo }}
            </div>
            <div class="telcel">
                <strong>Telcel: </strong>{{ $empleado->telcel }}
            </div>
            <div class="telefono">
                <strong>Telefono: </strong>{{ $empleado->telefono }}
            </div>
            <div class="telext">
                <strong>Telext: </strong>{{ $empleado->telext }}
            </div>
            <div class="whatsapp">
                <strong>Whatsapp: </strong>{{ $empleado->whatsapp }}
            </div>
            <div class="cargo">
                <strong>Cargo: </strong>{{ $empleado->cargo->descripcion }}
            </div>
            <div class="cargo">
                <strong>Empresa: </strong>{{ $empleado->empresa->razon_social ?? "" }} ({{ $empleado->empresa->ruc ?? "" }}) 
            </div>
            <div class="cargo">
                <strong>Cargo: </strong>{{ $empleado->cargo->descripcion }}
            </div>
            <div class="estado">
                <strong>Estado: </strong>{{ $empleado->estado->descripcion ?? "" }}  
            </div>
        </div>
    </div>  
  </div>    
@endsection;